from print_text import *

def test_stdout(capfd):
    result = print_text("Hello World!")
    out, err = capfd.readouterr()
    assert out == "Hello World!" and result == 202
