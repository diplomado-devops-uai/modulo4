import sys
import pytest

#Funcion definida: recibe un texto y lo imprime en la salida standard
def print_text(text):
    sys.stdout.write(text)
    return 202

# Test 1: retorna 202 la funcion?
def test_OK():
    assert print_text("DOING TEST 1") == 202

#Test 2: retorna 404 la función?
def test_NOT_OK():
    assert print_text("DOING TEST 2") == 404
